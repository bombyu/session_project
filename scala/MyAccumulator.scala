package day01

import org.apache.spark.util.AccumulatorV2
import sessionanalyze.constant.Constants
import sessionanalyze.util.StringUtils

/*
                    .::::.
                  .::::::::.
                 :::::::::::
             ..:::::::::::'	  FUCK YOU
           '::::::::::::'		Goddess bless, never BUG
             .::::::::::
        '::::::::::::::..
             ..::::::::::::.
           ``::::::::::::::::
            ::::``:::::::::'        .:::.
           ::::'   ':::::'       .::::::::.
         .::::'      ::::     .:::::::'::::.
        .:::'       :::::  .:::::::::' ':::::.
       .::'        :::::.:::::::::'      ':::::.
      .::'         ::::::::::::::'         ``::::.
  ...:::           ::::::::::::'              ``::.
 ```` ':.          ':::::::::'                  ::::..
                    '.:::::'                    ':'````..
                    
 ━━━━━━━━━━━━━━━━━━━━ 女神保佑,永无BUG ━━━━━━━━━━━━━━━━━━━━
*/
class MyAccumulator extends AccumulatorV2[String,String]{
  //定义一个初始值
  private var res = Constants.TIME_PERIOD_1s_3s + "=0|" +
                    Constants.TIME_PERIOD_4s_6s + "=0|" +
                    Constants.TIME_PERIOD_7s_9s + "=0|" +
                    Constants.TIME_PERIOD_10s_30s + "=0|" +
                    Constants.TIME_PERIOD_30s_60s + "=0|" +
                    Constants.TIME_PERIOD_1m_3m + "=0|" +
                    Constants.TIME_PERIOD_3m_10m + "=0|" +
                    Constants.TIME_PERIOD_10m_30m + "=0" +
                    Constants.TIME_PERIOD_30m + "=0"
  //如果这个累加器返回值为0，可以设置为false，否则会报错 java.lang.AssertionError: assertion failed: copyAndReset must return a zero value copy
  override def isZero: Boolean = {true}

  //拷贝一个当前对象
  override def copy(): AccumulatorV2[String, String] = {
    val acc  = new MyAccumulator
    acc.res = this.res
    acc
  }

  //重置数据
  override def reset(): Unit = res = Constants.TIME_PERIOD_1s_3s + "=0|" +
                                      Constants.TIME_PERIOD_4s_6s + "=0|" +
                                      Constants.TIME_PERIOD_7s_9s + "=0|" +
                                      Constants.TIME_PERIOD_10s_30s + "=0|" +
                                      Constants.TIME_PERIOD_30s_60s + "=0|" +
                                      Constants.TIME_PERIOD_1m_3m + "=0|" +
                                      Constants.TIME_PERIOD_3m_10m + "=0|" +
                                      Constants.TIME_PERIOD_10m_30m + "=0|" +
                                      Constants.TIME_PERIOD_30m + "=0"

  //操作数据累加方法
  override def add(v: String): Unit ={
    val v1 = res
    val oldValue = StringUtils.getFieldFromConcatString(v1,"\\|",v)
    if(oldValue != null){
      val value = oldValue.toInt + 1
      res = StringUtils.setFieldInConcatString(v1,"\\|",v,value+"")
    }
  }

  //合并数据
  override def merge(other: AccumulatorV2[String, String]): Unit = other match {
    case map:MyAccumulator => res = other.value
    case _ => throw new RuntimeException(
      s"执行自定义AccumulatorV2类的merge方法异常"
    )
  }

  //对外访问的数据结果
  override def value: String = res
}

