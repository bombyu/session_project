package sessionanalyze.dao;

import sessionanalyze.domain.PageSplitConvertRate;

/**
 * 页面切片转换率DAO接口
 * @author Administrator
 *
 */
public interface IPageSplitConvertRateDAO {

	void insert(PageSplitConvertRate pageSplitConvertRate);
	
}
