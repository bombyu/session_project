package day02

/*
                    .::::.
                  .::::::::.
                 :::::::::::
             ..:::::::::::'	  FUCK YOU
           '::::::::::::'		Goddess bless, never BUG
             .::::::::::
        '::::::::::::::..
             ..::::::::::::.
           ``::::::::::::::::
            ::::``:::::::::'        .:::.
           ::::'   ':::::'       .::::::::.
         .::::'      ::::     .:::::::'::::.
        .:::'       :::::  .:::::::::' ':::::.
       .::'        :::::.:::::::::'      ':::::.
      .::'         ::::::::::::::'         ``::::.
  ...:::           ::::::::::::'              ``::.
 ```` ':.          ':::::::::'                  ::::..
                    '.:::::'                    ':'````..
                    
 ━━━━━━━━━━━━━━━━━━━━ 女神保佑,永无BUG ━━━━━━━━━━━━━━━━━━━━
*/
//自定义排序类（点击数量、下单事件、支付事件）
class CategorySortKey(val clikCount:Long,val orderCount:Long,val payCount:Long) extends Comparable[CategorySortKey] with Serializable {
  override def compareTo(that: CategorySortKey): Int = {
    if(clikCount != that.clikCount) return (clikCount - that.clikCount).toInt
    else if(orderCount != that.orderCount) return (orderCount - that.orderCount).toInt
    else return (payCount - that.payCount).toInt
  }
}
